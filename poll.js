const Telegraf = require('telegraf') // import telegraf lib
const Markup = require('telegraf/markup') // Get the markup module
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const WizardScene = require('telegraf/scenes/wizard')

const bot = new Telegraf('742812092:AAFB6tuJwxUmPTgttN3uIeN3fNWwRHWPSZ4') // Get the token from the environment variable

const stage = new Stage()

bot.use(session())
bot.use(stage.middleware())
bot.startPolling() // Start polling bot from you computer

// Start Bot
bot.start(ctx => {
  ctx.reply(
    `How can I help you, ${ctx.from.first_name}?`,
    Markup.inlineKeyboard([
      Markup.callbackButton('💱 Start new poll', 'START_POLL'),
      Markup.callbackButton('🤑 View current polls', 'VIEW_POLLS')
    ]).extra()
  )
})

// poll creator Wizard
const pollViewer = new WizardScene(
  'poll_viewer',
  ctx => {
    // -> LOAD_FROM_IOTA_TANGLE
    ctx.wizard.state.polls = 'IOTA_GET_POLLS'
    ctx.reply(ctx.wizard.state.polls)
    ctx.reply(
      `Would you like to vote on a poll?, ${ctx.from.first_name}?`,
      Markup.inlineKeyboard([
        Markup.callbackButton('Vote on poll', 'VOTE_ON_POLL'),
        Markup.callbackButton('🤑 Extra', 'EXTRA')
      ]).extra()
    )
    return ctx.wizard.next()
  },
  ctx => {
    return ctx.scene.leave()
  }
)

const proposalVoting = new WizardScene(
  'proposal_voting',
  ctx => {
    ctx.reply(
      `Got it, your poll title is:  ${
        ctx.wizard.state.pollTitle
      } `
    )
    return ctx.wizard.next()
  }

)

// poll creator Wizard
const pollCreator = new WizardScene(
  'poll_creator',
  ctx => {
    ctx.reply('Please, formulate your poll')
    ctx.wizard.state.pollProposalCount = 0
    ctx.wizard.state.pollProposals = []
    return ctx.wizard.next()
  },
  ctx => {
    // only set pollTitle from user input, if no Proposals were given
    if (ctx.wizard.state.pollProposalCount == 0) {
      ctx.wizard.state.pollTitle = ctx.message.text
    }
    ctx.reply(
      `Got it, your poll title is:  ${
        ctx.wizard.state.pollTitle
      } `
    )
    ctx.reply(
      `\n Current proposals: ${ctx.wizard.state.pollProposals}`
    )
    ctx.reply(
      `\n What proposals would you like to add? - type END to finalize poll!`
    )
    // Go to the following scene
    return ctx.wizard.next()
  },
  ctx => {
    let maxProposals = 5
    if (ctx.message.text != 'END' && ctx.wizard.state.pollProposalCount < maxProposals) {
      ctx.wizard.state.pollProposals.push(ctx.message.text)
      ctx.wizard.state.pollProposalCount++

      console.log(ctx.wizard.state.pollProposals)

      ctx.wizard.back()  // Set the listener to the previous function
      return ctx.wizard.steps[ctx.wizard.cursor](ctx)
    } else {
      ctx.reply(
        `Poll listed:  ${
          ctx.wizard.state.pollTitle
        } `
      )
      ctx.reply(
        `\n Current proposals: ${ctx.wizard.state.pollProposals}`
      )
      ctx.reply(
        `\n Create new poll with /start`
      )
      // -> SAVE_TO_IOTA_TANGLE
      return ctx.scene.leave()
    }
  }
)

// Scene registration
stage.register(pollViewer)
stage.register(pollCreator)

bot.action('VIEW_POLLS', ctx => {
  ctx.scene.enter('poll_viewer')
})

bot.action('START_POLL', ctx => {
  ctx.scene.enter('poll_creator')
})

bot.action('VOTE_ON_POLL', ctx => {
  ctx.reply(
    `\n Current votes: `
  )
})

// bot.command('VIEW_POLLS', Stage.enter('poll_viewer')) // this works
