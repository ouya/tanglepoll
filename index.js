// ************* telegram bot **************************************************
const Telegraf = require('telegraf')
const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')

const bot = new Telegraf('742812092:AAFB6tuJwxUmPTgttN3uIeN3fNWwRHWPSZ4')

const sayYoMiddleware = ({ reply }, next) => reply('yo').then(() => next())
// bot.use(Telegraf.log());

// Require the use of IOTA library
const Iota = require('@iota/core')
const Converter = require('@iota/converter')
const ExtractJSON = require('@iota/extract-json')

const regex = /^\/([^@\s]+)@?(?:(\S+)|)\s?([\s\S]*)$/i

let isPoll = null

// Create a new instance of the IOTA class object.
// Use 'provider' variable to specify which Full Node to talk to
const iota = Iota.composeAPI({ provider: 'https://nodes.devnet.iota.org:443' })

// bot.on('newPoll', (ctx) => {
//   console.log(ctx.message)
// })

const keyboard = Markup.inlineKeyboard([
  Markup.urlButton('❤️', 'http://telegraf.js.org'),
  Markup.callbackButton('Delete', 'delete')
])

// bot.on('message', (ctx) => {
//   if (ctx.message.text == 'newPoll') {
//     return ctx.reply('Title:', Extra.markdown())
//   }
// }
// )

// bot.on(/.+/, sayYoMiddleware, (ctx) => {
//   console.log(ctx.message.text)
//   return ctx.reply('*42*', Extra.markdown())
// })
// bot.hears('hi', (ctx) => ctx.reply('Hey there'))
// bot.command('modern', ({ reply }) => reply('Yo'))

bot.command('onetime', ({ reply }) =>
  reply('One time keyboard', Markup
    .keyboard(['/simple', '/inline', '/pyramid'])
    .oneTime()
    .resize()
    .extra()
  )
)

bot.on('inline_query', (ctx) => {
  // here we got a location in lat/lng from device - TODO check for browser solution

  // get nearby events from tangle (by lat/lng tag .. AND TODO by tag search MUSIC,SPORTS)

  var result = []
  if (ctx.inlineQuery.location) {
    result = [
      { type: 'article', id: '1', title: 'RESULT 1', input_message_content: { message_text: ctx.inlineQuery.location.longitude } },
      { type: 'article', id: '2', title: 'RESULT 2', input_message_content: { message_text: ctx.inlineQuery.location.latitude } }
    ]
  } else {
    result = [
      { type: 'article', id: '1', title: 'RESULT 1', input_message_content: { message_text: 'res1' } },
      { type: 'article', id: '2', title: 'RESULT 2', input_message_content: { message_text: 'res2' } }
    ]
  }

  // console.log(ctx.inlineQuery.location.longitude)

  // show combobox in telegram
  ctx.answerInlineQuery(result)
})

bot.command('menu', ({ reply }) => {
  return reply('Custom buttons keyboard', Markup
    .keyboard([
      ['🔍 Search1', '😎 Popular'], // Row1 with 2 buttons
      ['☸ New', '📞 Feedback'], // Row2 with 2 buttons
      ['📢 Ads', '⭐️ Rate us', '👥 Share'] // Row3 with 3 buttons
    ])
    .oneTime()
    .resize()
    .extra()
  )
})

bot.hears('🔍 Search', ctx => ctx.reply('Yay!'))
bot.hears('📢 Ads', ctx => ctx.reply('Free hugs. Call now!'))

bot.command('special', (ctx) => {
  return ctx.reply('Special buttons keyboard', Extra.markup((markup) => {
    return markup.resize()
      .keyboard([
        markup.contactRequestButton('Send contact'),
        markup.locationRequestButton('Send location')
      ])
  }))
})

bot.command('pyramid', (ctx) => {
  return ctx.reply('Keyboard wrap', Extra.markup(
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      wrap: (btn, index, currentRow) => currentRow.length >= (index + 1) / 2
    })
  ))
})

bot.command('simple', (ctx) => {
  return ctx.replyWithHTML('<b>Coke</b> or <i>Pepsi?</i>', Extra.markup(
    Markup.keyboard(['Coke', 'Pepsi'])
  ))
})

bot.command('iotapoll', (ctx) => {
  return ctx.reply('<b>NewPoll</b> or <i>Search?</i>', Extra.HTML().markup((m) =>
    m.inlineKeyboard([
      m.callbackButton('NewPoll', 'NewPoll'),
      m.callbackButton('Search', 'Search')
    ])))
})

bot.command('inline', (ctx) => {
  return ctx.reply('<b>NewPoll</b> or <i>Search?</i>', Extra.HTML().markup((m) =>
    m.inlineKeyboard([
      m.callbackButton('NewPoll', 'NewPoll'),
      m.callbackButton('Search', 'Search')
    ])))
})

bot.command('poll', (ctx) => {
  isPoll = 'poll'
  // return ctx.reply('<b>NewPoll</b> or <i>Search?</i>', Extra.HTML().markup((m) =>
  //   m.inlineKeyboard([
  //     m.callbackButton('NewPoll', 'NewPoll'),
  //     m.callbackButton('Search', 'Search')
  //   ])))
})

bot.command('random', (ctx) => {
  return ctx.reply('random example',
    Markup.inlineKeyboard([
      Markup.callbackButton('Coke', 'Coke'),
      Markup.callbackButton('Dr Pepper', 'Dr Pepper', Math.random() > 0.5),
      Markup.callbackButton('Pepsi', 'Pepsi')
    ]).extra()
  )
})

bot.command('caption', (ctx) => {
  return ctx.replyWithPhoto({ url: 'https://picsum.photos/200/300/?random' },
    Extra.load({ caption: 'Caption' })
      .markdown()
      .markup((m) =>
        m.inlineKeyboard([
          m.callbackButton('Plain', 'plain'),
          m.callbackButton('Italic', 'italic')
        ])
      )
  )
})

bot.hears(/\/wrap (\d+)/, (ctx) => {
  return ctx.reply('Keyboard wrap', Extra.markup(
    Markup.keyboard(['one', 'two', 'three', 'four', 'five', 'six'], {
      columns: parseInt(ctx.match[1])
    })
  ))
})

bot.action('Dr Pepper', (ctx, next) => {
  return ctx.reply('👍').then(() => next())
})

bot.action('plain', async (ctx) => {
  ctx.editMessageCaption('Caption', Markup.inlineKeyboard([
    Markup.callbackButton('Plain', 'plain'),
    Markup.callbackButton('Italic', 'italic')
  ]))
})

bot.action('italic', (ctx) => {
  ctx.editMessageCaption('_Caption_', Extra.markdown().markup(Markup.inlineKeyboard([
    Markup.callbackButton('Plain', 'plain'),
    Markup.callbackButton('* Italic *', 'italic')
  ])))
})

bot.action(/.+/, (ctx) => {
  if (ctx.match[0] == 'NewPoll') {
    // Use a random seed as there is no tokens being sent.
    const seed =
        'PEEOTSEITFEVEWCWBTSIZM9NKRGJEIMXTULBACGFRQK9IMGICLBKW9TTEVSDQMGWKBXPVCBMMCXWMNPDX'

    // Create a variable for the address we will send too
    const address =
        'IOTAPOLLADDRESSTEST99999999999999999999999999999999999999999999999999999999999999'

    let iotaPoll = {}
    iotaPoll.id = 42
    iotaPoll.topic = 'When dinner'
    iotaPoll.proposal = 'Dinner at 8?'
    iotaPoll.options = ['Ja ', 'Nein']
    iotaPoll.visibility = 'public'

    const message = Converter.asciiToTrytes(JSON.stringify(iotaPoll))

    const transfers = [
      {
        value: 0,
        address: address, // Where the data is being sent
        tag: 'IOTAPOLL',
        message: message // The message converted into trytes
      }
    ]

    iota
      .prepareTransfers(seed, transfers)
      .then(trytes => iota.sendTrytes(trytes, 3, 9))
      .then(bundle => {
        console.log('Transfer successfully sent')
        bundle.map(tx => console.log(tx))
      })
      .catch(err => {
        console.log(err)
      })
  }

  if (ctx.match[0] == 'Search') {
    iota
      .findTransactions({ addresses: ['IOTAPOLLADDRESSTEST99999999999999999999999999999999999999999999999999999999999999'] })
      .then(hashes => {
        iota
          .getBundle(hashes[0])
          .then(bundle => {
            console.log(bundle[0].address)
            const msg = JSON.parse(ExtractJSON.extractJson(bundle))
            console.log(msg)
          })
          .catch(err => {
            console.log(err)
          })
      })
      .catch(err => {
        console.log(err)
      })
  }

  return ctx.answerCbQuery(`Oh, ${ctx.match[0]}! Great choice`)
})

bot.startPolling()
