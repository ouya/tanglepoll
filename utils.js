// Require the use of IOTA library
const Iota = require('@iota/core')
const Converter = require('@iota/converter')
// const ExtractJSON = require('@iota/extract-json')
const iotaAreaCodes = require('@iota/area-codes')
const iota = Iota.composeAPI({ provider: 'https://nodes.devnet.iota.org:443' })

const cassandra = require('cassandra-driver')
const client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1'
})

// item creator Wizard

// itemName: value.itemName,
// description: value.description,
// OPTIONAL listDays: value.listDays,
// OPTIONAL AUTO address: this.props.myPlaces.properties.label,
// AUTO lng: this.props.myPlaces.geometry.coordinates[0],
// AUTO lat: this.props.myPlaces.geometry.coordinates[1],
// AUTO_TELEGRAM_USER user: Meteor.userId(),
// AUTO_TELEGRAM_USER username: Meteor.user().username,
// AUTO_TELEGRAM_CAMERA imageId: this.props.imageId,
// OPTIONAL endDate: new Date(Date.now() + value.listDays * 24*60*60*1000)

const opts = {
  reply_markup: JSON.stringify({
    keyboard: [
      [{ text: 'Location', request_location: true }]
    ],
    resize_keyboard: true,
    one_time_keyboard: true
  })
}

const distance = (lat1, lon1, lat2, lon2) => {
  if ((lat1 === lat2) && (lon1 === lon2)) {
    return 0
  } else {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
    if (dist > 1) {
      dist = 1
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    dist = dist * 1.609344
    return dist
  }
}

const queryTransactions = async iac => {
  if (!iotaAreaCodes.isValidPartial(iac)) {
    throw new Error('The IOTA Area Code must be a full partial with 9 characters')
  }
  const iacArray = iac.split('')
  const pairs = iacArray.indexOf('A') / 2

  // Beginning query
  let query = `SELECT tx_id, iac, message FROM txDB.transaction WHERE`

  // Construct query
  Array(pairs)
    .fill()
    .map((_, i) => {
      query = query + ` pair${i} = '${iacArray[i * 2] + iacArray[i * 2 + 1]}'`
      if (pairs - 1 != i) {
        query = query + ' AND'
      } else {
        query = query + ';'
      }
    })
  console.log(query)
  let res = await client.execute(query)
  delete res.info
  return {
    success: true,
    items: res.rows
  }
}

const tangleStore = (item, iac) => {
  const seed =
      'PEEOTSEITFEVEWCWBTSIZM9NKRGJEIMXTULBACGFRQK9IMGICLBKW9TTEVSDQMGWKBXPVCBMMCXWMNPDX'

  // Create a variable for the address we will send too
  const address =
      'IOTAPOLLADDRESSTEST99999999999999999999999999999999999999999999999999999999999999'

  // item.name, item.description
  const message = Converter.asciiToTrytes(JSON.stringify(item))

  const transfers = [
    {
      value: 0,
      address: address, // Where the data is being sent
      tag: iac,
      message: message // The message converted into trytes
    }
  ]

  iota
    .prepareTransfers(seed, transfers)
    .then(trytes => iota.sendTrytes(trytes, 3, 9))
    .then(bundle => {
      console.log('Transfer successfully sent')
      bundle.map(tx => console.log(tx))
    })
    .catch(err => {
      console.log(err)
    })
}

module.exports = { opts, distance, queryTransactions, tangleStore }
