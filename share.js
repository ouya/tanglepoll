const iotaAreaCodes = require('@iota/area-codes')
const Converter = require('@iota/converter')
const { opts, distance, queryTransactions, tangleStore } = require('./utils')

const Telegraf = require('telegraf')
const Markup = require('telegraf/markup') // Get the markup module
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const Extra = require('telegraf/extra')

const bot = new Telegraf('742812092:AAFB6tuJwxUmPTgttN3uIeN3fNWwRHWPSZ4')
const stage = new Stage()
const WizardScene = require('telegraf/scenes/wizard')
bot.use(session({ inlineMode: true }))
bot.use(stage.middleware())
// bot.use(Telegraf.log())

bot.on('inline_query', async (ctx) => {
  if (ctx.inlineQuery.query === 'start' || ctx.inlineQuery.query === 'menu' || ctx.inlineQuery.query === 'help') {
    ctx.telegram.sendMessage(ctx.from.id, 'Type /start to start bot').then((result) => {
    }).catch((err) => {
      console.log(err)
    })
  }
  console.log(ctx.inlineQuery.query)
  let results = []
  let encodeIAC = iotaAreaCodes.encode(parseFloat(ctx.inlineQuery.location.latitude), parseFloat(ctx.inlineQuery.location.longitude))
  let iac = iotaAreaCodes.setPrecision(encodeIAC, 2)
  let response = await queryTransactions(iac)
  console.log(response)
  if (response.success) {
    // remove non-printable and other non-valid JSON chars
    // s = s.replace(/[\u0000-\u0019]+/g, '')
    // console.log(JSON.parse(Converter.trytesToAscii(response.items[0].message + '9').replace(/[\u0000-\u0019]+/g, '')))
    results = response.items.map((item) => ({
      type: 'article',
      dist: distance(ctx.inlineQuery.location.latitude, ctx.inlineQuery.location.longitude, iotaAreaCodes.decode(item.iac).latitude, iotaAreaCodes.decode(item.iac).longitude),
      id: item.tx_id.slice(0, 7),
      title: JSON.parse(Converter.trytesToAscii(item.message + '9').replace(/[\u0000-\u0019]+/g, '')).name,
      description: JSON.parse(Converter.trytesToAscii(item.message + '9').replace(/[\u0000-\u0019]+/g, '')).description + ' km : ' + distance(ctx.inlineQuery.location.latitude, ctx.inlineQuery.location.longitude, iotaAreaCodes.decode(item.iac).latitude, iotaAreaCodes.decode(item.iac).longitude),
      input_message_content: {
        message_text: JSON.parse(Converter.trytesToAscii(item.message + '9').replace(/[\u0000-\u0019]+/g, '')).name,
        parse_mode: 'Markdown' // HTL
      } }))
    results = results.filter(item => item.title.toUpperCase().indexOf(ctx.inlineQuery.query.toUpperCase()) !== -1)
    results.sort((a, b) => a.dist - b.dist)
  }

  ctx.answerInlineQuery(results)
})

bot.on('photo', (ctx) => {
  console.log(ctx.message.photo[0].file_id)
  ctx.reply('Welcome to TangleShare! Take a pic of your item!', Markup
    .keyboard([
      ['🔍 Search Space'],
      ['📦 Share item'],
      ['🍏 Share food'],
      ['📌 Set location']
    ]).oneTime().resize().extra()
  )
  // console.log(ctx.message.photo[0].file_id) // small size
  // ctx.telegram.sendPhoto(443173308, 'AgADAgADBawxG-_FEUqSleSenHJcOZHRUQ8ABO_hBq9XJskXmJACAAEC')
})

bot.on('chosen_inline_result', (ctx) => {
  console.log(ctx.chosenInlineResult)
  // console.log(ctx.session.results.find(x => x.id === ctx.chosenInlineResult.result_id).title)
  if (true) {
    console.log('yours')
  } else {
    // contact user
    ctx.telegram.sendMessage(ctx.from.id, 'I need your BAUM').then((result) => {
    }).catch((err) => {
      console.log(err)
    })
  }
})

bot.command('start', ({ reply }) => {
  return reply('Welcome to TangleShare! Take a pic of your item!', Markup
    .keyboard([
      ['🔍 Search Space'],
      ['📦 Share item'],
      ['🍏 Share food'],
      ['📌 Set location']
    ]).oneTime().resize().extra()
  )
})

const locationMenu = Telegraf.Extra
  .markdown()
  .markup((m) => m.inlineKeyboard([
    m.callbackButton('Use my location', 'myGPS'),
    m.callbackButton('Use other location', 'otherGPS')
  ]))

// bot.hears('📞 Back', ctx => ctx.telegram.sendMessage(ctx.chat.id, `/start\x0D`))
bot.hears('🔍 Search Space', ctx => ctx.scene.enter('SEARCH'))
bot.hears('📦 Share item', ctx => ctx.scene.enter('SHARE_ITEM'))
bot.hears('🍏 Share food', ctx => ctx.scene.enter('SHARE_FOOD'))
bot.hears('📌 Set location', ctx => ctx.reply('Choose Location', locationMenu).then(() => {
}))
// ctx.reply('Choose Location', locationMenu).then(() => {
// })

bot.on('location', (ctx) => {
  ctx.session.location = ctx.message.location
  ctx.reply(`Your location is:` + ctx.session.location.latitude + `/` + ctx.session.location.longitude)
})

// bot.on('edited_message', (ctx) => {
//   console.log(ctx.editedMessage.location)
// })

const SHARE_ITEM = new WizardScene(
  'SHARE_ITEM',
  ctx => {
    ctx.reply('Item name: ')
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item = {}
    ctx.session.item.name = ctx.message.text
    ctx.reply(`Item description:`)
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item.description = ctx.message.text
    if (ctx.session.location) {
      ctx.session.item.userId = ctx.chat.id // save userId
      const iac = iotaAreaCodes.encode(ctx.session.location.latitude, ctx.session.location.longitude)
      tangleStore(ctx.session.item, iac)
      ctx.reply(`Item saved!`)
    } else {
      ctx.reply(`Please give location first!`)
    }
    return ctx.scene.leave()
  }
)

const SHARE_FOOD = new WizardScene(
  'SHARE_FOOD',
  ctx => {
    ctx.reply('Item name: ')
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item = {}
    ctx.session.item.name = ctx.message.text
    ctx.reply(`Item description: `)
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item.description = ctx.message.text
    return ctx.scene.leave()
  }
)

const CREATE_EVENT = new WizardScene(
  'CREATE_EVENT',
  ctx => {
    ctx.reply('Event name: ')
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item = {}
    ctx.session.item.name = ctx.message.text
    ctx.reply(`Event description: ${ctx.session.event.name}`)
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item.description = ctx.message.text
    ctx.reply('Choose Location', locationMenu).then(() => {
    })
    return ctx.scene.leave()
  }
)

const SEARCH = new WizardScene(
  'SEARCH',
  ctx => {
    // ctx.reply(`Your location is:` + ctx.session.location.latitude + `/` + ctx.session.location.longitude)
    ctx.reply(`\n GridSize: Type X for xlarge (2200km), L for (110km), M for (5.5km), S for (275m)`)
    return ctx.wizard.next()
  },
  async (ctx) => {
    ctx.reply(`\n Searching ... `)
    switch (ctx.message.text) {
      case 'X': ctx.session.gridChar = 2; break
      case 'L': ctx.session.gridChar = 4; break
      case 'M': ctx.session.gridChar = 6; break
      case 'S': ctx.session.gridChar = 8; break
      default: ctx.reply(`\n Max grid selected `); ctx.session.gridChar = 2; break
    }
    let encodeIAC = iotaAreaCodes.encode(parseFloat(ctx.session.location.latitude), parseFloat(ctx.session.location.longitude))
    let iac = iotaAreaCodes.setPrecision(encodeIAC, ctx.session.gridChar)
    console.log(iac)

    let response = await queryTransactions(iac)
    if (response.success) {
      ctx.reply(`Nb of items found: , ${response.items.length}`)
      for (var i = 0; i < response.items.length; i++) {
        let outputLine = Converter.trytesToAscii(response.items[i].message + '9')
        let areaCode = iotaAreaCodes.decode(response.items[i].iac)
        let dist = distance(ctx.session.location.latitude, ctx.session.location.longitude, areaCode.latitude, areaCode.longitude)
        if (outputLine.length < 4096) {
          ctx.reply(outputLine + ` at ` + dist + ` km`)
        } else {
          ctx.reply('Message too long for this dataset - max 4096 characters')
        }
      }
    }
    return ctx.scene.leave()
  }
)

// Scene registration
stage.register(SHARE_ITEM)
stage.register(SHARE_FOOD)
stage.register(CREATE_EVENT)
stage.register(SEARCH)

bot.launch()
